GitLab CLI (Python) Utilities forked from https://gitlab.com/mcepl/create-gitlab-repo 
-----------------------------------------------------------------------------------------------
1) create-gitlab-repo:  Simple python script which creates a project on the gitlab site defined in ~/.config/gitlab configuration file.

2) get_project_id:  Simple python script to get project ID of the current repository on gitlab.com.
